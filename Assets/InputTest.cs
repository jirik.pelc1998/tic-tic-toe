using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTest : MonoBehaviour
{
    Vector2 speed;

    void Update()
    {
        this.transform.position += new Vector3(speed.x, speed.y) * Time.deltaTime;
    }

    public void OnMovement(UnityEngine.InputSystem.InputAction.CallbackContext context)
	{
        speed = context.ReadValue<Vector2>();
	}

    public void OnAction(UnityEngine.InputSystem.InputAction.CallbackContext context)
	{
        GetComponent<SpriteRenderer>().color = new Color(Random.Range(0f,1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
	}
}
